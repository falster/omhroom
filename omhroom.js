(function($) {
	$(document).ready(function() {
		$('#calendar').fullCalendar({
			schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
			now: moment().format('YYYY-MM-DDTHH:mm:ss'),
			nowIndicator: true,
			locale: 'nn',
			editable: true,
			selectable: true,
			eventLimit: true, // allow "more" link when too many events
			aspectRatio: 1.8,
			eventOverlap: false,
			titleFormat: "dddd Do MMMM YYYY",
			scrollTime: '08:00', // undo default 6am scrollTime
			header: {
				left: 'today prev,next',
				center: 'title',
				right: 'agendaDay,timelineThreeDays,agendaWeek,month,listWeek'
			},
			defaultView: 'agendaDay',
			views: {
				timelineThreeDays: {
					type: 'timeline',
					duration: {
						days: 3
					}
				}
			},
			resourceLabelText: 'Møterom',
			resources: Drupal.settings.resources,
			events: '/admin/calendar/events',
			select: function(start, end, jsEvent, view, resource) {
				user = Drupal.settings.user;
				name = Drupal.settings.user.name;
				firm = Drupal.settings.user.firm;
				title = name + ' (' + firm + ')';
				upevent(
					title,
					start.format(),
					end.format(),
					resource.id,
					0,
					Drupal.settings.user.uid
				);
			},
			loading: function(isLoading, view) {
				//console.log('Loading: ' + isLoading);
			},
			dayClick: function(date, jsEvent, view, resource) {
				console.log(
					'dayClick',
					date.format(),
					resource ? resource.id : '(no resource)'
				);
			},
			eventResize: function(event, delta, revertFunc) {
				fmat = 'YYYY-MM-DDTHH:mm:ss';
				start = moment(event._start._d).utc().format(fmat);
				end = moment(event._end._d).utc().format(fmat);
				upevent(
					event.title,
					start,
					end,
					event.resourceId,
					0,
					Drupal.settings.user.uid,
					event._id
				);
			},
			eventClick: function(calEvent, jsEvent, view) {
				eventdialog(calEvent, jsEvent, view);
			},
			eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) {
				fmat = 'YYYY-MM-DDTHH:mm:ss';
				start = moment(event._start._d).utc().format(fmat);
				end = moment(event._end._d).utc().format(fmat);
				upevent(
					event.title,
					start,
					end,
					event.resourceId,
					0,
					Drupal.settings.user.uid,
					event._id
				);
			}
		});

		var form = $('#eventform');
		var title_field = $('#eventform .title');
		var body_field = $('#eventform .desc');
		var nid_field = $('#eventform .nid');
		var start_d = $('#eventform .date_start');
		var start_t = $('#eventform .time_start');
		var end_d = $('#eventform .date_end');
		var end_t = $('#eventform .time_end');
		var date_fields = $('#eventform .form-date');
		var room_field = $('#eventform .rooms');

		Messenger.options = {
			extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
			theme: 'flat',
		}

		function eventdialog(calEvent, jsEvent, view) {
			fdate = 'YYYY-MM-DD';
			ftime = 'HH:mm';
			date_s = moment(calEvent._start._d).utc().format(fdate);
			date_e = moment(calEvent._end._d).utc().format(fdate);
			time_s = moment(calEvent._start._d).utc().format(ftime);
			time_e = moment(calEvent._end._d).utc().format(ftime);
			if (calEvent.editable == true) {
				start_d.val(date_s);
				start_t.val(time_s);
				end_d.val(date_e);
				end_t.val(time_e);
				nid_field.val(calEvent.id);
				$("#eventform input[name=rooms][value=" + calEvent.resourceId + "]").prop('checked', true);
				title_field.val(calEvent.title).focus(function() {
					$(this).select();
				});
				check_occupied();
				showmsg('#eventform');
			} else {
				if (date_s == date_e) {
					date = date_s;
				} else {
					date = date_s + ' - ' + date_e;
				}
				$.each(Drupal.settings.resources, function(key, value) {
					if (calEvent.resourceId == value.id) {
						room = value.title;
					}
				});
				time = time_s + ' - ' + time_e;
				msg = time + ' <br /> ' + date + '<br />' + room;
				showdialog(calEvent.title, msg);
			}
		}

		$(document).keyup(function(e) {
			if (e.keyCode == 27) {
				hidemsg('#eventform');
				hidemsg('#error');
			}
		});

		$('#eventform #edit-submit').click(function(event) {
			event.preventDefault();
			lock_form(true);
			start = start_d.val() + 'T' + start_t.val();
			end = end_d.val() + 'T' + end_t.val();
			room = $("#eventform  input:radio[name=rooms]:checked").val();
			room_title = room_name(room);
			form_ok = true;

			if (start.length < 16 && end.length < 16) {
				form_ok = false;
				Messenger().error('Du må sette ett tidspunkt!');
			}

			if (moment(start).format('X') > moment(end).format('X')) {
				form_ok = false;
				Messenger().error('Slutt-dato må være senere enn startdato.');
			}

			if (title_field.val().length < 1) {
				form_ok = false;
				Messenger().error('Du må sette en tittel! En booking er nødt til å hete noe.');
			}

			if (typeof room == 'undefined') {
				form_ok = false;
				Messenger().error('Du må velge et rom, en booking må være et sted.');
			}

			if (form_ok) {
				Messenger().post({
					message: 'Undersøker om ' + room_title + ' er ledig ...',
					type: 'info',
					id: 'up'
				});

				occupied = null;

				$.post('/admin/calendar/occupied', {
					start: start,
					end: end,
					room: room,
					nid: nid_field.val()
				}, function(data) {
					occupied = data;
				}, 'json');

				var occupied_status = function() {
					if (occupied == true || occupied == false) {
						if (occupied != true) {
							form_ok = false;
							Messenger().post({
								message: room_title + ' er opptatt på det tidspunktet. Velg et annet tidspunkt, eller et annet rom.',
								type: 'error',
								id: 'up'
							});
						}

						if (form_ok) {
							upevent(
								title_field.val(),
								start,
								end,
								room,
								body_field.val(),
								0,
								nid_field.val()
							);
							hidemsg('#eventform');
							clearform();
						}
						lock_form(false);
					} else {
						setTimeout(function() {
							occupied_status()
						}, 1000);
					}
				};
				occupied_status();
			} else {
				lock_form(false);
			}
		});

		$(date_fields).change(function() {
			check_occupied();
		});

		$('#eventform #edit-delete').click(function(event) {
			event.preventDefault();
			nid = $('#eventform .nid').val();
			title = title_field.val();
			hidemsg('#eventform');
			clearform();
			destroy_event(nid, title);
		});

		$('#eventform #edit-cancel').click(function(event) {
			event.preventDefault();
			hidemsg('#eventform');
			clearform();
		});

		function lock_form(bool) {
			$('input[type="submit"]').attr('disabled', bool);
		}

		function clearform() {
			$('#eventform input[type=text]').each(function(i) {
				var $this = $(this);
				$this.val('');
			});
			$('#eventform input[type=date]').each(function(i) {
				var $this = $(this);
				$this.val('');
			});
			$('#eventform input[type=time]').each(function(i) {
				var $this = $(this);
				$this.val('');
			});
			$('#eventform .rooms').each(function(i) {
				var $this = $(this);
				$this.removeAttr('checked');
				$this.attr('disabled', false);
			});
			lock_form(false);
		}

		function check_occupied() {
			start = start_d.val() + 'T' + start_t.val();
			end = end_d.val() + 'T' + end_t.val();

			$('#eventform .rooms').each(function(i) {
				var $this = $(this),
					tid = $this.val();
				$.post('/admin/calendar/occupied', {
					start: start,
					end: end,
					room: tid,
					nid: nid_field.val()
				}, function(data) {
					if (data == false) {
						$this.attr('disabled', true);
						$this.parent().addClass('occupied');
					} else {
						$this.attr('disabled', false);
						$this.parent().removeClass('occupied');
					}
				}, 'json');
			});
		}

		function check_occupied_connect(start, end, tid, id) {
			$.post('/admin/calendar/occupied', {
				start: start,
				end: end,
				room: tid,
				nid: id
			}, function(data) {
				return data;
			}, 'json');
		}

		function room_name(room_id) {
			rooms = Drupal.settings.resources;
			$.each(rooms, function(index, value) {
				if (room_id == value.id) {
					name = value.title;
				}
			});
			return name;
		}

		function upevent(title, start, end, room, desc, uid, id) {
			id = id || 0;
			uid = uid || 0;
			room_title = room_name(room);
			Messenger().post({
				message: 'Din booking av ' + room_title + ' blir prosessert ...',
				type: 'info',
				id: 'up'
			});

			if (Offline.state == 'down') {
				Messenger().post({
					message: 'Du har tilkoblingsproblemer med internettet. Din booking vil bli automatisk forsøkt gjennomført når forbindelsen er tilbake.',
					type: 'info',
					id: 'up'
				});
			}

			$.post('/admin/calendar/add', {
				title: title,
				nid: id,
				start: start,
				end: end,
				room: room,
				uid: uid,
				body: desc,
			}, function(data, status) {
				if (data.status == true) {
					refetch();
					if (data.new == true) {
						Messenger().post({
							message: 'Bookingen "' + title + '" i ' + room_title + ' ble opprettet',
							type: 'success',
							id: 'up',
							showCloseButton: true
						});
					} else {
						Messenger().post({
							message: 'Bookingen "' + title + '" i ' + room_title + ' er oppdatert',
							type: 'success',
							id: 'up',
							showCloseButton: true
						});
					}
				} else if (data.status == false) {
					Messenger().post({
						message: room_title + ' er desverre opptatt på tidspunkt!',
						type: 'error',
						id: 'up',
						showCloseButton: true
					});
					refetch();
				}
			}, 'json');
		}


		function destroy_event(id, title) {
			$.post('/admin/calendar/delete', {
				nid: id,
			}, function() {
				refetch();
				Messenger().success('Bookingen "' + title + '" ble slettet og vil aldri bli snakket om igjen.');
			});
		}

		function refetch() {
			$('#calendar').fullCalendar('refetchEvents');
		}

		function showmsg(id) {
			$('body').addClass('curtain');
			$(id).show();
			fheight = $(id + ' fieldset').outerHeight();
			$(id).height(fheight);
		}

		function hidemsg(id) {
			$(id).hide();
			$('body').removeClass('curtain');
		}

		function showdialog(title, msg) {
			$('#error h2').html(title);
			$('#error .msg').html(msg);
			showmsg('#error');
		}

		$('#error #edit-submit').click(function(event) {
			event.preventDefault();
			hidemsg('#error');
		});

		Offline.on("up", refetch);

	});
})(jQuery);