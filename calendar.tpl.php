<?php
$options = ''; 
foreach ($variables['data']['resources'] as $room) {
	//$options .= '<option value="'.$room['id'].'">'.$room['title'].'</option>';
	$options .= '<div class="form-item form-type-radio">';
	$options .= '<input type="radio" id="field_room_'.$room['id'].'" name="rooms" value="'.$room['id'].'" class="rooms form-radio">  <label class="option" for="field_room_'.$room['id'].'">'.$room['title'].'<small> (Opptatt)</small></label>';
	$options .= '</div>';
}
//$select = '<select name="rooms" class="eventform rooms form-select">' . $options . '<select>';
?>
<div id="calendar"></div>
<div id="wrapper">
	<form id="eventform" class="centerdialog">
	  <fieldset>
	  	<h2>Rediger event</h2>
	    <input type="text" name="title" placeholder="Tittel" class="eventform title form-text">
	    <label for="date_start">Fra</label>
	    <input type="date" name="date_start" class="eventform date_start form-text form-date">
	    <input type="time" name="time_start" class="eventform time_start form-text form-date">
	    <label for="date_end">Til</label>
	    <input type="date" name="date_end" class="eventform date_end form-text form-date">
	    <input type="time" name="time_end" class="eventform time_end form-text form-date">
	<!--    <label for="date_start">Beskrivelse</label>
	   <textarea class="eventform desc text-full form-textarea" name="body[und][0][value]" cols="40" rows="3"></textarea>-->
	   	<label for="rooms">Rom</label>
	   	<?php print $options; ?>
	    <input type="text" name="nid" class="eventform nid hidden">
	    <div class="form-actions form-wrapper" id="edit-actions">
	    	<input type="submit" id="edit-submit" name="op" value="Lagre" class="form-submit">
	    	<input type="submit" id="edit-cancel" name="op" value="Avbryt" class="form-submit">
	    	<input type="submit" id="edit-delete" name="op" value="Slett" class="form-submit">
	    </div>
	  </fieldset>
	</form>
	<div id="error" class="centerdialog">
		<fieldset>
			<h2>Feilmelding</h2>
			<div class="msg">Det har skjedd en feil av typen 42</div>
			<div class="form-actions form-wrapper" id="edit-actions">
	    	<input type="submit" id="edit-submit" name="op" value="Lukk" class="form-submit">
	    </div>
		</fieldset>
	</div>
</div>