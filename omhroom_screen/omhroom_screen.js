(function($) {
	$(document).ready(function() {


		room_status();

		setInterval(room_status, 10000);

		function room_status() {
			now = moment().format('YYYY-MM-DDTHH:mm:ss');
			$('.content .room').each(function(i) {
				var $this = $(this);
				tid = $this.data('room');
				$.post('/admin/calendar/occupied', {
					start: now,
					end: now,
					room: tid
				}, function(data) {
					if (data == true) {
						$this.addClass('open');
						$this.removeClass('closed');
					} else {
						$this.removeClass('open');
						$this.addClass('closed');
					}
				}, 'json');
			});

			event_list();

		}

		function event_list() {
			now = moment().format('YYYY-MM-DDTHH:mm:ss');
			start_day = moment().utc().format('YYYY-MM-DDT[00:00:00]');
			end_day = moment().utc().format('YYYY-MM-DDT[23:59:59]');
			event_ids = [];
			$.post('/admin/calendar/events', {
				start: start_day,
				end: end_day,
			}, function(data) {

				event_ids = [];
				$.each(data, function(i, item) {
					event_ids[item.resourceId] = [];
				});

				$.each(data, function(i, item) {
					room_id = item.resourceId;
					x_now = moment().unix() + (moment().utcOffset() * 60);
					x_start = moment(item.start).format('X');
					x_end = moment(item.end).format('X');
					HH_start = moment(item.start).utc().format('HH:mm');
					HH_start_m = '<span class="t-start">' + HH_start + '</span>';
					HH_end = moment(item.end).utc().format('HH:mm');
					HH_end_m = '<span class="t-end">' + HH_end + '</span>';
					markup = '<li data-event="' + item.id + '" data-room="' + room_id + '" data-time="' + x_start + '">' + HH_start_m + ' til ' + HH_end_m + '<br/><span class="title">' + item.title + '</span></li>';


					$room = $('#room-' + room_id);

					if (x_end < x_now) {
						st = 0; // Past
					} else if ((x_start < x_now) && (x_end > x_now)) {
						st = 1; // Occuring
					} else if (x_start > x_now) {
						st = 2; // Future
					}

					current = $('li[data-event="' + item.id + '"][data-room="' + room_id + '"]');

					if (st) {
						event_ids[room_id].push(parseInt(item.id));
						if (!current.length > 0) {
							$room.children('.events').append(markup);
						} else {
							current.children('.t-start').html(HH_start);
							current.children('.t-end').html(HH_end);
							current.children('.title').html(item.title);
						}

						if (st == 1) {
							$('li[data-event="' + item.id + '"][data-room="' + room_id + '"]').addClass('occuring');
						} else {
							$(current).removeClass('occuring');
						}
					}

				});

				$('.room').each(function(i, index) {
					room_id = $(this).data('room');
					$('li[data-room="' + room_id + '"]').each(function(i, index) {
						event_id = $(this).data('event');
						event_id = parseInt(event_id);
						if (jQuery.inArray(event_id, event_ids[room_id]) == -1) {
							$(this).slideUp("normal", function() {
								$(this).remove();
							});
						}
					});
				});

			}, 'json');


		}
	});
})(jQuery);